<?php

// separate code invoked by the og2mlm_sympa_og2mlm_ function
// extends the og2mlm abstract connection object for sympa

class og2mlm_connection extends abstract_og2mlm_connection {
 
  private $soapUser;
  private $soapPassword;
  private $md5;
  private $soapclient_fix;
  private $robot;
  public  $owner; // email address of owner, all actions taken as list owner

  public function __construct($server, $domain) {
    // get the server parameters
    $this->owner = variable_get('og2mlm_dropbox_address', '');
    $this->sid = $server['sid'];
    $this->soapServer = $server['soapServer'];
    $this->soapUser = $server['admin'];
    $this->soapPassword = $server['password'];
    $this->debug = $server['debug'];
    $this->robot = $domain;
    $this->soapclient_fix = new soapclient_fix($this->soapServer,'wsdl');
  }

  // run the sympa equivalent of the request via the soap client
  private function _soap_cmd($sympa_command, $params = NULL) {
    // set arguments to 'authenticateRemoteAppAndRun'
    $args = array($this->soapUser, $this->soapPassword,'USER_EMAIL='.$this->owner, $sympa_command, $params);
    $result = $this->soapclient_fix->call('authenticateRemoteAppAndRun', $args);
    if ($this->debug) {
      watchdog('og2mlm_sympa',t('Soap cmd with args %args : %result', array('%args' => var_export($args,TRUE),'%result' => var_export($result,TRUE))));
    }
  }

  // list creation/removal
  
  public function create_list($listname, $param)  {
    return $this->_soap_cmd('createList', array($listname, $param->subject, 'private_working_group', $param->description, $param->topics));
  }
 
  public function remove_list($listname, $ignore = NULL) {
    return $this->_soap_cmd('closeList', array($listname));
  } 
 
  public function retrieve_list($listname, $ignore = NULL) {
    return $this->_soap_cmd('info', array($listname));
  }

  // list membership
  public function add_member($listname, $useremail) {
    $gecos = ''; $quiet = TRUE;
    return $this->_soap_cmd('add', array($listname, $useremail, $gecos, $quiet));
  }
 
  public function remove_member($listname, $useremail) {
    $quiet = TRUE;
    return $this->_soap_cmd('del', array($listname, $useremail, $quiet));
  }

  // update for all available lists on this server!
  // possibly somewhat barbaric, i'm ignoring the domain ..
  public function update_member($useremail, $newemail) {
    db_set_active('og2mlm_sympa_'.$this->sid);
    db_query('UPDATE subscriber_table SET user_subscriber = "%s" WHERE user_subscriber = "%s"', $newemail, $useremail);
    // get rid of duplicates
    db_query('DELETE FROM subscriber_table WHERE user_subscriber = "%s"', $useremail);
    db_query('UPDATE user_table SET email_user = "%s" WHERE email_user = "%s"', $newemail, $useremail);
    db_query('DELETE FROM user_table WHERE email_user = "%s"', $useremail);
    db_set_active('default');
  }
 
  // return a list of all members of a list, excluding members of $group

  public function get_members($listname, &$group) {

    $sympa  = array();
    $db = 'og2mlm_sympa_'.$this->sid;
    db_set_active($db);
    $result = db_query("SELECT user_subscriber FROM subscriber_table WHERE list_subscriber = '%s' AND robot_subscriber = '%s' ORDER BY user_subscriber", $listname, $this->robot);
    while ($row = db_fetch_array($result) ) {
      $subscriber = $row['user_subscriber'];
      if ($group[$subscriber]) {
        unset($group[$subscriber]);
      }
      else {
        $sympa[$subscriber] = 1;
      }
    }
    db_set_active();
    return $sympa;
  }

}

?>
