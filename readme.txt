DESCRIPTION
--------------------------
Links Organic Groups to a Mailing List Manager (MLM).  Messages posted to an organic group will be sent to a specified MLM which will propagate that message to all subscribed users.  

This module was created for use with FLOSS-DGROUPS.

REQUIREMENTS
-------------
-og2mlm requires phpimap to be enabled
-organic groups must be installed and enabled


INSTALLATION
---------------
-Make sure Organic Groups is installed before installing og2mlm. (do not forget to set admin >> settings >> og access control to enable!)
- Activate the og2mlm, og2mlm_messages AND og2mlm_basic modules as usual. If you are using sympa as your MLM, activate og2mlm_sympa.
-Visit the admin/settings/og2mlm page and fill out the required settings:
The way og2mlm synchronizes email messages and organic group posts is by subscribing a dropbox email address to each mailing list, leaving the mlm to handle the messages appropriately.  A new email account should be created for this purpose and set here.  For the moment the address must be pop3 on port 110 (default).
Hostname: hostname of the email address.  Probably something like "mail.og2mlm.com"
Email dropbox: email address of the dropbox.  e.g. "og_dropbox@og2mlm.com"
Password: Password for the email dropbox.
Min. Polling Interval: og2mlm polls the email dropbox whenever a user views an organic group.  To cut down strain on og2mlm and the email dropbox, it is recommended you set this value somewhere above 10 seconds.
Max # of messages to retrieve: If there is a lot of mailing list activity, set this to a reasonable number to cut down wait times when viewing groups.  Between 10 and 50 should be reasonable if the Min. polling interval is around 30 seconds.
MLM Hostname: The hostname of your mailing list manager.  For example if one of your mailing lists is "foolist@bar.com", you would enter "bar.com" as the hostname.

-If og2mlm_sympa is enabled, proceed to admin/settings/og2mlm_sympa:
In order for og2mlm to talk with sympa, sympa must be set up to allow SOAP calls.  Refer to http://www.sympa.org/doc/html/node13.html and http://www.sympa.org/wiki/soap_extention/soap_extention for more information about setting up sympa with soap extensions.  Note that trusted_applications.conf must also be set up properly. With "proxy_for_variables" set to "USER_EMAIL"

trusted_applications.conf example
---------------------------------
name og2mlm
md5password acbd18db4c.......4fccc4a4d8
proxy_for_variables USER_EMAIL

In this example, username og2mlm is allowed access to sympa soap using the password '....'.  ('acbd18db4c.......4fccc4a4d8' is the md5 hash of '....')

If this all works then set the following in admin/settings/og2mlm_sympa:
Sympa SOAP server address: This is the wsdl file that tells og2mlm how to make rpc calls to sympa using SOAP.
Sympa's demo is at http://demo.sympa.org/sympa/wsdl and yours will probably look something like http://yourdomain.com/sympa/wsdl
Sympa administrator: This it the trusted application that has been set in trusted_applications.conf.  In the case of the preceding example, this would be set to 'og2mlm'.
Sympa administrator password: This is the password for the trusted application.  In the case of the preceding example, this would be set to '....'.
-If you are using the Dgroups theme, then go to admin/blocks and replace 'Group details' with 'Group details (og2mlm)'
-Now you can create your first mlm associated organic group by going to node/add/og
  -Set your usual og settings and note that there is now an extra field, 'Mailing list name:' which must be filled in order to relate that group to a mailing list.
-You should now be able to select 'create message' for that organic group.  Once that message is submitted it will be sent to the mlm, and from there to the dropbox.  It will take at least 'Min interval' until that message is loaded into the group.
-You may also wish to set up a cron job to poll for new messages at regular intervals: http://drupal.org/cron

NOTES
----------------
- Dedicate a single reliable email address to be the dropbox for og2mlm.  Check your mlm from time to time to make sure messages aren't being lost.
- A watchdog message is sent to the admin panel whenever new message(s) have been received.  
- If the events and fileshare modules are installed, a default event and default fileshare will be created with each group.


CREDITS
----------------------------
Authored by Martin Eckart and Steve McCullough (openconcept.ca)
Contributing modules: mailhandler, listhandler, og2list
